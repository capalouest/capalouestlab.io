# Cap à l'ouest oooOOOH YEAH

On se sert d'Hugo, un générateur de site statique malin, pour générer le site internet à partir de fichiers markdown (1).   
Nous hébergeons le site sur Gitlab, une plateforme git(2) en ligne (http://gitlab.com), grâce à leur fonctionnalité Gitlab Pages, 
qui permet de créer un site web à part de fichiers statiques.

(1) Git est une sorte de système de versionning décentralisé.   
Nous créons, mettons à jour des fichiers que nous mettons ensuite sur un répertoire git en ligne, dans notre cas Gitlab.   
Si tu te sens motivé, ou si tu te fais simplement chier: https://git-scm.com/documentation. J'explique néanmoins les commandes de base plus bas.

(2) Markdow est un format de mise en page pour le non codeurs (PUF PUF bouseux PUF). Tu trouveras ici une petite explication de comment ça fonctionne: https://guides.github.com/features/mastering-markdown/

## Pour modifier capalouest.me en pleine tempête, hors ligne

## Prérequis:
 - Git installé
 - Hugo intallé


### Windobe
 L'installation de Git est simple: https://git-scm.com/download/win.
 Télécharge et execute le .exe, installation par défaut. Tu peux utiliser Bash.exe installé par Git à la place de l'invite de commande windows. C'est la même chose, mais en mieux foutu.
 L'installation de Hugo est un peu plus chiante: https://gohugo.io/.
 Tu peux suivre ce tutorial: https://gohugo.io/tutorials/installing-on-windows/.
 Il s'agit de télécharger le "binary" (ce n'est pas un executable, mais un fichier utilisable entre autre que par l'invite de commande, ou Bash de git), de le mettre dans un dossier (par exemple C:\Hugo\bin) ainsi que de le renommer simplement 'hugo'.
 A partir de ce moment, il suffit de taper dans la commande ``$ C:\Hugo\bin\hugo`` pour l'utiliser.
 Par exemple, ``$ C:\Hugo\bin\hugo version`` affichera la version d'Hugo.
 Biensûr, devoir taper le chemin pour le binary d'Hugo à chaque fois peut être contraignant (même si appuyer sur la flèche du haut quand on est dans le terminal permet de réécrire la dernière commande, et ainsi de s'épargner de tout réécrire).
 Mais il y a des possibilités pour rendre Hugo disponible sous la simple commande `$ hugo` grâce au variables d'environnement. Les explications sont disponibles dans le tutoriel donné plus haut.

### Mac
 Mac est cool. Donc si on installe Homebrew (package manager pour mac => http://brew.sh/), il suffit de faire "brew udpate & brew install git & brew install hugo" dans le terminal et HOP. Dans ta gueule Windows.

Via git, tu vas télécharger/mettre à jour le site sur ton ordi (cette partie nécéssite une connexion).
Avec Hugo, tu vas ajouter des contenus, des "posts", et crééer un serveur local pour voir les changements que tu fous (toute cette partie ne nécéssite pas de connexion, juste un cerveau).
Et tu vas après, via git, donner une explications à ces changements et les "pousser" en ligne (donner une explication sert à savoir les derniers changements qui ont été faits, et permet de revenir en arrière quand nécéssaire).

## Utilisation

Le principe est donc dans télécharger le site localement, hors ligne, sur ton ordi.

### Tu n'as pas le site sur ton ordi, you DONUT.
Si tu n'as pas ou plus le site sur ton ordi, il faut cloner la version princpiale, existante sur Gitlab.
Il faut pour cela aller dans le dossier dans lequel tu veux télécharger ton site (dans ton cas, vu qu'on a mis en place Hugo sur Windows, la place idéale est C:\Hugo\Sites, mais on peut évidemment le mettre où on veut).

> Note: Pour naviguer dans des dossiers dans l'invite de commande, ou dans le terminal pour mac, tout ce passe avec la commande `$ cd`. ``$ cd un/dossier/quelconque`` va naviguer, dans le terminal, jusqu'à ce dossier.
La façon la plus simple et d'aller écrire `$ cd ` dans le terminal, et de glisser-déposer le dossier désiré depuis l'exporateur de fichier, ou finder pour mac.
Vous verrez le chemin du dossier se coller automatiquement dans le terminal, il suffira alors d'appuyer sur enter.
Voilà un lien si tu veux en apprendre plus sur les commandes unix (et donc mac): https://doc.ubuntu-fr.org/tutoriel/console_commandes_de_base.
Cela fonctionne aussi pour windows, quand on utilise git Bash (qui émule un terminal unix)

Dans le bon dossier, il faut alors bourrer un ``$ git clone https://gitlab.com/capalouest/capalouest.gitlab.io.git`` dans le terminal. Boum.

### Tu as déjà le site sur ton ordinateur, smartass.

Là, dans le dossier de ton site, il suffit de faire `$ git pull ` afin de juste mettre à jour par rapport aux derniers changements qui auraient été faits sur le serveur dans ton dos.
Si tu oublies, ce n'est pas un drame, tu risques juste des conflits (tu pourrais par exemple modifier un fichier que quelqu'un aurait également modifié pour probablement t'emmerder).

### Tu veux modifier ton site

C'est là qu'Hugo rentre en scène mon vieux.   
Dans le dossier de ton site , tapes `$ hugo server`. Hugo va te créer un serveur. Il te dira même quelle sera l'addresse à mettre dans ton Internet explorer 6.
> Note: Créer un serveur n'est pas obligatoire, mais il te permet de voir les changements opérés avant de mettre en ligne

N'oublie pas, les posts en markdown se situent dans capalouest.io/content/post.
> Note: Un fichier mardown est un simple fichier texte qui a comme extension .md ou .mardown, rien de plus a faire.

Créer simplement un fichier markdown fonctionnerait, mais n'oublie pas le Front matter (les paramètres délimités par les +++ au dessus du fichier, nécéssaires à tout posts).
Si tu ne veux pas l'oublier, hugo te permet des les ajouter pour toi via la commande `$ hugo new post/le-nom-de-mon-post.md`.

Voici à quoi ressemble un post type;

```
+++
image_vignette = "/images/photos/IMG_0155_vignette.jpg"
image_full = "/images/photos/IMG_0155.jpg"
date = "2016-10-15T13:56:08+02:00"
title = "Visite des Saintes"
+++

# Pourquoi sommes nous allés là bas ?

Pour les boobies et le rhum yohooooo
```
Fais toi plaisir.   
N'oublie pas de vérifier sur ta version locale que tu ne fous pas la merde.

Pour le moment, les posts sont un peu la seule chose que tu sais modifier.   
Si tu veux pouvoir modifier le reste, je m'en peinturlure les samosas avec la peinture de l'indifférence. 

### Tu veux mettre tes modifications en ligne

Là, tu remets hugo de coté. Tu dois juste dire à git quel sont les fichiers que tu veux mettre à jour dans le site original.  
`$ git status` est ton ami, il te permet de savoir où git en est.    
Pour dire à git quels sont les changements (en gros, les ajouter à l'index de la prochaine mise à jour), tu dois faire un `$ git add lefichier/enquestion.xxx`.   
Heureusement, vu que tu vas ajouter 30 posts par jour, le raccourci `$ git add --all` permet d'ajouter à l'index tous les changements à la fois.

Ensuite, tu dois dire à git le contenu de ta mise à jour (ce qu'on appelle un commit).  
`$ git commit -m "explication de mon commit"`
> Note: `-m` est un raccourci pour accrocher un message directement sur la même ligne, sinon, essaye, c'est broutant.

Voilà, tu as fait tes mises à jour, tu les as ajoutées à l'index de git et tu as créé une mise à jour, il faut désormais taper `$ git push` pour pousser les modifications sur le serveur GitLab. Celui-ci va le détecter, et construire le site internet sous base de tes changements.

### Résumé

`$ git clone https://gitlab.com/capalouest/capalouest.gitlab.io.git`   
ou   
`$ git pull`   
`$ hugo server`   
`$ hugo new post/truc-interessant.md`   
`$ git add --all`   
`$ git commit -m "g fé une mise a jour lol"`   
`$ git push`   

## Pour modifier directement en ligne, you horse fart.

Tu peux directement tout faire en ligne.  
Le seul problème sera que tu seras aveugle, pas moyen de voir tes modifications avant de mettre en ligne. Tu vas potentiellement tout pèter, faire une dépression et appeler papa Chips au secours.
Tu sais bien que je passe trop de temps dans mon club de striptease et que j'ai pas de temps pour ces conneries.   
Aussi, tu n'auras pas l'aide d'hugo pour ajouter les front matter à ta place.

Sinon, rien de plus simple;   
rends toi sur la page de ton projet: https://gitlab.com/capalouest/capalouest.gitlab.io, connecte toi avec ton compte capalouest.   
Cliques sur le lien **Files (x MB)**. Tu verras la version "principale" de ton site. Comme localement, cliques sur **content** et **post**. Là, copie-colles un front matter d'un autre post, et cliques sur le **+** et ajoutes un fichier (n'oublies pas de l'appeler .md). Et fais ton taff.

Tu remarqueras qu'il n'y a pas de Sauvergarder mais bien un Commit. Tu vas vraiment faire en ligne ce que tu aurais fait localement.  
Bref, là vous devez partir pour votre dernière soirée, je vais donc abbréger et terminer cette documentation un autre jour.

---

Je t'aime mon petit loulou, bisou sur chaque fesse. :heart:

![samosas](http://i.giphy.com/10UUe8ZsLnaqwo.gif)
